﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrayOfValues = new int[20];
            int highestVal;
            int lowestVal;
            int sumVal;
            int averageVal;

            int realLength = FillArray(ref arrayOfValues);
            CalculateArray(out highestVal, out lowestVal, out sumVal, out averageVal, arrayOfValues, realLength);

            Console.Write("Your array was: ");
            for (int i = 0; i < realLength; i++)
            {
                Console.Write("{0}, ", arrayOfValues[i]);
            }
            Console.WriteLine();
            Console.WriteLine("highest value: {0}", highestVal);
            Console.WriteLine("lowest value: {0}", lowestVal);
            Console.WriteLine("sum of values: {0}", sumVal);
            Console.WriteLine("average of values: {0}", averageVal);
        }

        static int FillArray(ref int[] arrayOfValues)
        {
            int i;
            for (i = 0; i < 20; i++)
            {
                Console.WriteLine("Enter a number or enter x to stop");
                string inputValue = Console.ReadLine();
                int canConvert;
                Int32.TryParse(inputValue, out canConvert);
                if (inputValue.ToLower() == "x")
                {
                    break;
                }
                else if (canConvert == 0)
                {
                    Console.WriteLine("Enter a valid interger or x");
                    i--;
                }
                else
                {
                    arrayOfValues[i] = Convert.ToInt32(inputValue);
                }
            }
            return i;
        }
        static void CalculateArray(out int highestVal, out int lowestVal, out int sumVal, out int averageVal, int[] arrayOfValues, int realLength)
        {
            //highest number
            highestVal = arrayOfValues[0];
            for (int i = 0; i < realLength; i++)
            {
                if (arrayOfValues[i] > highestVal)
                {
                    highestVal = arrayOfValues[i];
                }
            }
            //lowest number
            lowestVal = arrayOfValues[0];
            for (int i = 0; i < realLength; i++)
            {
                if (arrayOfValues[i] < lowestVal)
                {
                    lowestVal = arrayOfValues[i];
                }
            }
            //Sum of array
            sumVal = 0;
            for (int i = 0; i < realLength; i++)
            {
                sumVal += arrayOfValues[i];
            }
            //average of array
            averageVal = sumVal / realLength;
        }
    }
}

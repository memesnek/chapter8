﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX_7
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[80];
            int intParse;
            int i;
            for (i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine("Enter a number (press x to stop)");
                string userInput = Console.ReadLine();
                int.TryParse(userInput, out intParse);
                if (userInput.ToLower() == "x")
                {
                    break;
                }
                else if (intParse == 0)
                {
                    i--;
                    Console.WriteLine("That was not a number or a x");
                }
                else
                {
                    numbers[i] = intParse;
                }
            }
            DisplayAverages(numbers, i);
        }

        static void DisplayAverages(int[] arrayONums, int realLength)
        {
            int total = 0;
            for (int i = 0; i < realLength; i++)
            {
                total += arrayONums[i];
            }
            int average = total / realLength;

            Console.Write("you entered: ");
            for (int i = 0; i < realLength; i++)
            {
                Console.Write("{0}, ", arrayONums[i]);
            }
            Console.WriteLine();
            Console.WriteLine("The average of these numbers is {0}", average);
        }
    }
}

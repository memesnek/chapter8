﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX_10
{
    class Program
    {
        static void Main(string[] args)
        {
            int runningTime;
            Console.Write("enter movie name: ");
            string movieName = Console.ReadLine();
            Console.WriteLine("Enter running time of movie in minutes (leave blank if unknown)");
            string runningTimeString = Console.ReadLine();
            int.TryParse(runningTimeString, out runningTime);

            if (runningTime == 0)
            {
                DisplayMovieInfo(movieName);
            }
            else
            {
                DisplayMovieInfo(movieName, runningTime);
            }
        }

        static void DisplayMovieInfo(string movieName, int runningTime = 90)
        {
            Console.WriteLine("Movie name: {0}", movieName);
            Console.WriteLine("Running Time: {0}", runningTime);
        }
    }
}

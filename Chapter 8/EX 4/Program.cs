﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your bid: ");
            string inputVal = Console.ReadLine();
            int intParse;
            double dblParse;
            int.TryParse(inputVal, out intParse);
            double.TryParse(inputVal, out dblParse);

            if (intParse != 0)
            {
                ReadBid(intParse);
            }
            else if (dblParse != 0)
            {
                ReadBid(dblParse);
            }
            else
            {
                ReadBid(inputVal);
            }
        }
        static void ReadBid(int bidAmount)
        {
            if (bidAmount >= 10)
            {
                Console.WriteLine("Your bid of ${0} is valid", bidAmount);
            }
            else
            {
                Console.WriteLine("The minimum bid is $10 ${0} does not meet the minimum", bidAmount);
            }
        }
        static void ReadBid(double bidAmount)
        {
            if (bidAmount >= 10)
            {
                Console.WriteLine("Your bid of {0:C} is valid", bidAmount);
            }
            else
            {
                Console.WriteLine("The minimum bid is $10.00 {0:C} does not meet the minimum", bidAmount);
            }
        }
        static void ReadBid(string bidAmount)
        {
            if (bidAmount.StartsWith("$"))
            {
                double bid = Convert.ToDouble(bidAmount.Substring(1));
                if (bid >= 10)
                {
                    Console.WriteLine("Your bid of {0:C} is valid", bid);
                }
                else
                {
                    Console.WriteLine("The minimum bid is $10.00 {0:C} does not meet the minimum", bid);
                }
            }
            else if (bidAmount.EndsWith("dollars"))
            {
                double bid = Convert.ToDouble(bidAmount.Substring(0, bidAmount.Length - 7));
                if (bid >= 10)
                {
                    Console.WriteLine("Your bid of {0:C} is valid", bid);
                }
                else
                {
                    Console.WriteLine("The minimum bid is $10.00 {0:C} does not meet the minimum", bid);
                }
            }
            else
            {
                Console.WriteLine("Format incorrect");
            }
        }
    }
}
